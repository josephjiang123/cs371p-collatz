// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(1, 17);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(543345, 234234);
    ASSERT_EQ(v, 470);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(3456, 9999);
    ASSERT_EQ(v, 262);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(917, 222);
    ASSERT_EQ(v, 179);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(1, 4);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(4005, 233333);
    ASSERT_EQ(v, 443);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(4, 5);
    ASSERT_EQ(v, 6);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(3000, 10000);
    ASSERT_EQ(v, 262);
}

TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(19, 17);
    ASSERT_EQ(v, 21);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(4698, 99984);
    ASSERT_EQ(v, 351);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n2 5000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n2 5000 238\n", w.str());
}
